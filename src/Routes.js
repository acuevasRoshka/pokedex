import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PokelistContainer from './containers/PokeListContainer';
import PokeInfoContainer from './containers/PokeInfoContainer';

const Routes = () => {
    return(
        <Switch>
            <Route exact path='/' component={PokelistContainer}/>
            <Route path='/pokemons' component={PokelistContainer}/>
            <Route exact path='/poke-info/:pokeIndex/:nombre' component={PokeInfoContainer}/>
        </Switch>
    );
}

export default Routes;