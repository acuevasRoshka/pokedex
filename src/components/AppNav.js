import React from 'react';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles'

function AppNav(props){
    const {classes}=props;
    return(
        <AppBar className={classes.NavColor} position="static">  
            <Toolbar variant="dense"  > 
                <Typography variant="h6" component="p">Pokédex</Typography>
            </Toolbar>
        </AppBar>
    )
}
export default withStyles({
    NavColor: {
        backgroundColor: '#EF5350',
    }
})(AppNav);