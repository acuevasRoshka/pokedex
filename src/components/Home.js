import React from 'react';
import { Link } from 'react-router-dom';

const Home =()=>{

    return(
        <div>
            < header>
                <h1>Poke App</h1>
                <button>Ver Pokemons</button>
                <Link to='/pokemons'>Ver pokemons</Link>
            </header>
        </div>
    );

}
export default Home;