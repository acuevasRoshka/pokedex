import React,{Fragment} from 'react';
import PokeCard from './PokeCard';
import {Grid} from '@material-ui/core';

function List({pokedata}){
    return(
        <Fragment>
                <Grid container justify="center" spacing={1}> 
                    {pokedata.map((pokemon,index)=>{
                        let url = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
                        var array= pokemon.url.split('/');  
                        var pokeIndex=array[array.length-2];  
                        let direc = url+pokeIndex+'.png?raw=true'
                        console.log("Valor del pokemons: ",direc)
                        return (
                            <PokeCard to={`/poke-info/${pokeIndex}/${pokemon.name}`} name={pokemon.name} image={direc} id={pokeIndex} key={pokeIndex}/>
                        )
                    })}
                </Grid>
        </Fragment>
    );
}

export default List;