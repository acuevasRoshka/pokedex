import React, {Fragment} from "react";
import { Button } from "@material-ui/core";
const Pagination = ({increment,decrement,count}) =>{
    return (
        <Fragment>
            <Button onClick={decrement} variant="outlined" color="primary">Anterior</Button>
            <Button variant="contained" color="primary" >
                {count}
            </Button>
            <Button onClick={increment} variant="outlined" color="primary">Siguiente</Button>        
        </Fragment>
    )
}

export default Pagination; 