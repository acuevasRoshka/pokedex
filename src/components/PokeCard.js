import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import { IonCard } from '@ionic/react';
function PokeCard({name,classes,image,to=''}) {
    return(
        <IonCard className={classes.item} href={to}>
          <img src={image} height='60 px'/>
          <ion-card-header>            
            <ion-card-title>{name}</ion-card-title>
          </ion-card-header>
        </IonCard>
    )
}

export default withStyles({
    item:{
        minWidth:"180px",
        margin:"1em",
        boxSizing:"border-box",
        textAlign:"center"
    },
    media:{
        height: "250px"
    }
})(PokeCard);