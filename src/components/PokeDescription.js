import React from 'react';
import PokeCard from './PokeCard';

import { Card, CardContent,CardActionArea,Typography, Grid} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import {withStyles} from '@material-ui/core/styles';


function PokeDescription ({nombre,
                           classes,
                           imagen,
                           descripcion,
                           height,
                           weight,
                           types}){
    
    const [spacing] = React.useState(2);                               
    return(
        <Grid container className={classes.centerContainer} >
            <Grid item md={7} >
                <Card className={classes.descriptionContainer}>
                    <CardActionArea >
                        <img src={imagen} height='200px'/>
                        <Typography gutterBottom variant="h5" component="h2">
                            {nombre}
                        </Typography>           
                    </CardActionArea>
                </Card>
            </Grid>

            <Grid item md={7}>
                <div className={classes.descriptionContainer}>
                    <Paper className={classes.descriptionBox}>
                        <Typography variant='body1' component='p' className={classes.descriptionText}>
                            {descripcion}
                        </Typography> 

                        <div className={classes.habilidad}>
                            <Typography variant='body1' component='p' className={classes.descriptionText}>
                                Altura {height/10} m
                            </Typography>
                            <Typography variant='body1' component='p' className={classes.descriptionText}>
                                Peso {weight/10} Kg
                            </Typography>
                            {types.map((tipo,index)=>{
                                return(
                                    <Typography key={index} variant='body1' component='p' className={classes.descriptionText}>
                                        {tipo.type.name}
                                    </Typography>  
                                )                       
                            })}

                        </div>
                    </Paper>
                </div> 

                       
             
            </Grid>

                    
               

        </Grid>
    );
}

export default withStyles({
   centerContainer:{
    margin: '0 auto',
    width: '60% !important',
    textAlign: 'center',
   },
   descriptionContainer:{
       margin: '5px',
       minWidth: '100%', 
   },
   descriptionBox:{
       padding: '2em',
       height: 'auto',
   },
   descriptionText:{
       fontSize: '1em',
       textAlign: 'justify',
       fontFamily: 'Verdana'
   },
   habilidad:{
        margin: '20px',
        backgroundColor: '#30a7d7',
        padding:'10px',
        paddingLeft:'20px',
        color: '#fff',
        borderRadius:'10px'

        
   }



}) (PokeDescription);