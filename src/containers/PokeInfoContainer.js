import React, {Component} from 'react'
import PokeDescription from '../components/PokeDescription';
import axios from 'axios';
import AppNav from '../components/AppNav';

class PokeInfoContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            descripcionPokemon: "",
            nombre: "",
            pokeId: "",
            height: "",
            weight: "",
            types: []
        };
    }

    componentDidMount(){
        const { match } = this.props;
        const pokeId = match.params.pokeIndex;
        const nombre = match.params.nombre;
        const PokeDescriptionUrl = `${process.env.REACT_APP_POKE_API_BASE_URL}pokemon-species/${pokeId}/`
        this.getDescription();
        axios.get(PokeDescriptionUrl)
        .then(res => {   
            const { flavor_text_entries } = res.data;
            //Es lo mismo que esto:
            //descripcionPokemon: res.data.flavor_text_entries[11].flavor_text
            this.setState({
                descripcionPokemon: flavor_text_entries[11].flavor_text,
                nombre,
                pokeId
            })
        })
        .catch(error=>{
            console.log(error);
        })
    }

    getDescription(){
        const { match } = this.props;
        const pokeId = match.params.pokeIndex;
        const url = `${process.env.REACT_APP_POKE_API_BASE_URL}pokemon/${pokeId}`
        axios.get(url)
        .then(r=>{
            const {height,weight,types}=r.data;
            this.setState({
                height,
                weight,
                types
            })
        })
    }
    render(){
        const {descripcionPokemon,nombre,pokeId} = this.state;
        let url = `${process.env.REACT_APP_POKEMON_ART}`;
        const {height,weight,types} = this.state;
        return(
            <>
                <AppNav />
                <PokeDescription 
                    nombre={nombre}
                    imagen={`${url}${pokeId}.png?raw=true`}
                    descripcion={descripcionPokemon}
                    height={height}
                    weight={weight}
                    types={types}
                /> 
            </>
        );
    }
}
export default PokeInfoContainer;