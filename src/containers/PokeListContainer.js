import React ,{Component} from 'react'
import List from'../components/List';
import axios from 'axios';
import AppNav from '../components/AppNav'
import Pagination from '../components/Pagination';

class PokelistContainer extends Component {
   state = {
       pokeData:[],
       currentOffset:0,
       count:0,
   }
    componentDidMount() {
        this.fetchData();
    }
    componentDidUpdate(prevProps,prevState){
        const {currentOffset,count}=this.state;
        if (currentOffset != prevState.currentOffset){
            if(currentOffset<0){
                this.setState({
                    currentOffset:0,
                    count:0
                })
                this.fetchData(prevState.currentOffset);
            }
            this.fetchData(currentOffset);
        }
    }
    fetchData = (offset) => {  
        const url = `${process.env.REACT_APP_POKE_API_BASE_URL}pokemon`;   
        let params = {
            offset: offset,
            limit: 20 
        }    
        axios.get(url,{params})
        .then(res => {
            const {results} = res.data;
            this.setState({
                pokeData: results,
            })
        })
        .catch(error=>{
            console.log(error);
        })
    }

    increment = () => {
        const {currentOffset,count} = this.state;
        this.setState({
            currentOffset:currentOffset+20,
            count:count+1
        })
    }
    decrement = () => {
        const {currentOffset,count} = this.state;
        this.setState({
            currentOffset:currentOffset-20,
            count:count-1
        })
    }
    render(){
        const { pokeData,currentOffset, count } = this.state;
        console.log(currentOffset);
        return(
            <>
                <AppNav />
                <Pagination 
                    increment = {this.increment} 
                    decrement = {this.decrement} 
                    count = {count}
                />
                <List pokedata={pokeData}/>
            </>
        );
    }
}

export default PokelistContainer;